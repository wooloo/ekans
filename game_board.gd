extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var width = 40
export var height = 30
export var starting_length = 4
export var time_per_move = 1
export var time_per_food = 0.9
export var food_count = 1

onready var outer = get_node("csg/outer")
onready var inner = get_node("csg/inner")
onready var parent = get_node("parent")

var origin = Vector3(-width/2, 0.5, -height/2)
var positions = [];
var food_positions = [];
var foods = [];
var body_parts = [];
var head_scene = preload("res://head.tscn")
var body_scene = preload("res://body.tscn")
var food_scene = preload("res://food.tscn")

var lerping = false
var face_direction = 0
var move_timer = 0
var can_die_from_intersection = false

const move_vecs = [
	Vector2(1, 0),
	Vector2(-1,0),
	Vector2(0, 1),
	Vector2(0,-1)
]

# Called when the node enters the scene tree for the first time.
func _ready():
	outer.width = width + 2
	inner.width = width
	outer.height = height+2
	inner.height = height
	respawn();
	pass # Replace with function body.

func respawn():
	time_per_move = 1
	positions = [];
	food_positions = [];
	foods = [];
	body_parts = [];
	can_die_from_intersection = false;
	for i in range(0,starting_length):
		positions.push_back(Vector2(0,0))
	var new_x = round(rand_range(0, width))
	var new_y = round(rand_range(0, height))
	for i in range(0,starting_length):
		positions.push_front(Vector2(new_x,new_y))
	for i in parent.get_children():
		parent.remove_child(i)
		i.remove_and_skip()
	var head = head_scene.instance()
	parent.add_child(head)
	body_parts.push_back(head)
	for i in range(0,food_count):
		var food_x = round(rand_range(0, width))
		var food_y = round(rand_range(0, height))
		food_positions.push_back(Vector2(food_x, food_y))
		var food = food_scene.instance()
		parent.add_child(food)
		foods.push_back(food)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Increase timer
	move_timer += delta
	# Handle input
	if Input.is_action_just_pressed("ui_right") and face_direction != 0:
		face_direction = 0
	elif Input.is_action_just_pressed("ui_left") and face_direction != 1:
		face_direction = 1
	elif Input.is_action_just_pressed("ui_up") and face_direction != 2:
		face_direction = 2
	elif Input.is_action_just_pressed("ui_down") and face_direction != 3:
		face_direction = 3
	# Populate body parts
	while len(body_parts) < len(positions):
		var body = body_scene.instance()
		parent.add_child(body)
		body_parts.push_back(body)
	while len(positions) > len(body_parts):
		var part = body_parts.pop_back()
		parent.remove_child(part)
		part.remove_and_skip()
	# Place and rotate body parts
	for i in range(0, len(body_parts)):
		var new_pos = Vector3(
			positions[i].x - width/2,
			positions[i].y - height/2,
			0.5
		)
		body_parts[i].transform.origin = new_pos
		if i-1 >= 0:
			var relative = positions[i-1] - positions[i]
			var dir = relative.angle()
			body_parts[i].rotation.z = dir
	# Move
	if move_timer > time_per_move:
		var move_vec = move_vecs[face_direction]
		var new_pos = positions[0]+move_vec
		positions.push_front(new_pos)
		positions.pop_back()
		move_timer = 0
	# Place foods
	for i in range(0,len(food_positions)):
		var new_pos = Vector3(
			food_positions[i].x - width/2,
			food_positions[i].y - height/2,
			0.5
		)
		foods[i].transform.origin = new_pos
	# Do food logic
	for i in range(0, len(food_positions)):
		if food_positions[i] == positions[0]:
			print("Food collected!")
			var food_x = round(rand_range(0, width))
			var food_y = round(rand_range(0, height))
			food_positions[i] = Vector2(food_x,food_y)
			positions.push_back(positions[0])
			time_per_move *= time_per_food
			can_die_from_intersection = false
		var intersecting = true
		while intersecting:
			intersecting = false
			for o in positions:
				if food_positions[i] == o:
					var food_x = round(rand_range(0, width))
					var food_y = round(rand_range(0, height))
					food_positions[i] = Vector2(food_x,food_y)
					intersecting = true
	# Do death logic
	var existing = []
	for i in positions:
		if i.x < 0 or i.x > width or i.y < 0 or i.y > height:
			respawn()
		if !i in existing:
			existing.push_back(i)
	if len(existing) == len(positions):
		can_die_from_intersection = true
	elif can_die_from_intersection:
		respawn()
